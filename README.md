dotfiles
========

Setting up a new robot computer is a common occurence. Setting up terminal preferences is mundane. Use this repo to install terminal preferences and extra goodies.

## Features

### ROS Profiles

This is perhaps the most useful feature. Using ROS on computers with multiple ROS workspaces can be annoying. ROS profiles help manage different ROS workspaces and network setups.

To create a new ROS profile, edit the `~/.rosprofiles` file and follow the instructions there. Then, you can use the following commands:

- `ros_show`: prints the current profile information
- `rosws_none`: deselects a persistent workspace
- `rosnet_private`: sets the ROS network to be the local computer only
- `rosws_<your-workspace>`: selects the workspace you created
- `rosnet_<your-network>`: selects the network you created


### ROS `lastbag`

Finds the last bag in a directory and runs `rqt_bag THE_BAG`.

### Vim Settings and Extensions

- [YouCompleteMe](https://github.com/ycm-core/YouCompleteMe)
- [GitGutter](https://github.com/airblade/vim-gitgutter)

### tmux Settings and Extensions

Quality of life things like:

- mouse usage
- infinite scrollback
- change prefix from `<ctrl>-b` to `<ctrl>-a` (to be more like `screen`)

### Random Terminal Stuff

- aliases: `ll`, `l`, `..`, `...`, `llg`
- `PS1` is set to robot's' hostname
- `PS1` changes colors if logged in via SSH

## Installation

Clone. Run `./install.sh`. YouCompleteMe will take a while.

## Credits

https://github.com/plusk01/dotfiles
